#ifndef CWINDOWMAIN_H
#define CWINDOWMAIN_H

#include "EngineDraw.h"

#include <string>
#include <vector>
#include <windows.h>

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);


class CWindowMain
{
    public:
        std::vector<POINT> *points;

        CWindowMain();
        ~CWindowMain();
        bool Create(const HINSTANCE& hInstance);
        void Destroy();
        void Show(const int& nCmdShow);
        void EnableOpenGL(HDC* hDC, HGLRC* hRC);
        void DisableOpenGL (HDC hDC, HGLRC hRC);
        void Display();
        void ChangeDrawingObject(const TYPE_DRAW& typeDraw);
        void OnMouseMove(const WPARAM& wParam, const LPARAM& lParam);
        void OnMouseClick(const WPARAM& wParam, const LPARAM& lParam);
        void OnResize(const int& width, const int& height);
        void OnCreate(const HWND& hwnd);
        void OnUndo();
        void OnContinue();
        bool GetStatusUndo();
    protected:
    private:
        int m_height;
        int m_width;
        HWND m_hwnd;
        HMENU m_popupMenu;
        std::string m_name;
        CEngineDraw m_engineDraw;
};

#endif // CWINDOWMAIN_H

