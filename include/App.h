#ifndef CAPP_H
#define CAPP_H

#include "WindowMain.h"

class CApp
{
    public:
        static CApp& GetInstance();
        ~CApp();
        CWindowMain* GetWindowMain();
    protected:
    private:
        CWindowMain* m_windowMain;
        CApp();
};

#endif // CAPP_H

