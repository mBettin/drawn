#ifndef CPOINT_H
#define CPOINT_H


class CPoint
{
    public:
        CPoint();
        CPoint(double x, double y, double z);
        ~CPoint();
        double x;
        double y;
        double z;
    protected:
    private:
};

#endif // CPOINT_H
