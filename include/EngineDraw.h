#ifndef CENGINEDRAW_H
#define CENGINEDRAW_H

#include <vector>
#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>


#include "IDrawObject.h"
#include "Point.h"
#include "Until.h"

#include "DrawArc.h"
#include "DrawCircle.h"
#include "DrawLine.h"

class CEngineDraw
{
    public:
        CEngineDraw(const int& width, const int& height);
        ~CEngineDraw();
        void InitScene();
        void Display();
        void Reshape(const int& width, const int& height);
        void AddDrawObject();
        void SetCurrentPoint(const CPoint& point) {
            m_currentPoint = point;
        }
        void SetLastPoint() {
            m_lastPoint = m_currentPoint;
        }
        CPoint ConvertPosition(const int& x, const int&y);
        inline void SetCurrentTypeDraw(TYPE_DRAW typeDraw) { if (TYPE_DRAW_NONE == typeDraw) { m_isDraw =false; } m_currentTypeDraw = typeDraw; }
        void Undo();
        void ClearDraw();
        void Continue();
        bool IsDraw();
        void EnableDraw();
        void DisabledDraw();
    protected:
    private:
        TYPE_DRAW m_currentTypeDraw;
        bool m_isDraw;
        CPoint m_currentPoint;
        CPoint m_lastPoint;
        const int* m_width;
        const int* m_height;
        std::vector<IDrawObject*>* m_drawObjects;
        //scene
        GLfloat m_aspect;
        //drawing object
        CDrawArc* arc;
        CDrawLine* line;
        CDrawCircle* circle;


};

#endif // CENGINEDRAW_H
