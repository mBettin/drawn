#ifndef CDRAWLINE_H
#define CDRAWLINE_H

#include "IDrawObject.h"
#include "Point.h"

class CDrawLine : public IDrawObject
{
    public:
        CDrawLine();
        ~CDrawLine();
        void Draw();
        void CurrentDraw();
    protected:
    private:
};

#endif // CDRAWLINE_H
