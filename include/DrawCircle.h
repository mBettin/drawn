#ifndef CDRAWCIRCLE_H
#define CDRAWCIRCLE_H

#include "IDrawObject.h"
#include "Point.h"

class CDrawCircle : public IDrawObject
{
    public:
        CDrawCircle();
        ~CDrawCircle();
         void Draw();
         void CurrentDraw();

    protected:
    private:
        float m_Xs,m_Ys,m_r;
         void CalculateCircle(const CPoint& p1, const CPoint& p2, const CPoint &p3, float* Xs, float* Ys, float* r);
};

#endif // CDRAWCIRCLE_H
