#ifndef IDRAWOBJECT_H
#define IDRAWOBJECT_H

#include "Point.h"
#include "Until.h"

class IDrawObject
{
    public:
        IDrawObject(TYPE_DRAW typeDraw);
        virtual ~IDrawObject();
        virtual void Draw() = 0;
        virtual void CurrentDraw() = 0;
        virtual void SetPoints(CPoint beginPoint, CPoint endPoint);
        inline void SetTypeDraw(TYPE_DRAW typeDraw) { m_typeDraw = typeDraw; }
        inline TYPE_DRAW GetTypeDraw() { return m_typeDraw; }
        inline CPoint GetEndPoint() { return m_endPoint; }
        inline CPoint GetBeginPoint() { return m_beginPoint; }
    protected:
        CPoint m_beginPoint;
        CPoint m_endPoint;
    private:
        TYPE_DRAW m_typeDraw;

};

#endif // IDRAWOBJECT_H
