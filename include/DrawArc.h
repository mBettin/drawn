#ifndef CDRAWARC_H
#define CDRAWARC_H
#include <windows.h>
#include <gl/gl.h>  //kolejnosc nag�wk�w ma znaczenie
#include <gl/glu.h>

#include "IDrawObject.h"
#include "Point.h"

class CDrawArc : public IDrawObject
{
    public:
        CDrawArc();
        virtual ~CDrawArc();
        void Draw();
        void CurrentDraw();
        void SetPoints(CPoint beginPoint, CPoint endPoint);
    protected:
    private:
        GLUnurbsObj* m_nurbs;
        GLfloat* m_knots;
        GLfloat* m_controlPoints;
        int m_countControlPoints;

        void Init(int nCountControlPoints);
};

#endif // CDRAWARC_H
