#ifndef UNTIL_H_INCLUDED
#define UNTIL_H_INCLUDED

#define PI 3.14159265359
#define TPI 6.283185
#define VERTEX_3 3


enum TYPE_DRAW {
    TYPE_DRAW_NONE,
    TYPE_DRAW_LINE,
    TYPE_DRAW_ARC,
    TYPE_DRAW_CIRCLE
};

#endif // UNTIL_H_INCLUDED
