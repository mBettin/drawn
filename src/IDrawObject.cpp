#include "IDrawObject.h"
#include "Until.h"

IDrawObject::IDrawObject(TYPE_DRAW typeDraw) : m_typeDraw(typeDraw)
{
    //ctor
}

IDrawObject::~IDrawObject()
{
    //dtor
}

void IDrawObject::SetPoints(CPoint beginPoint, CPoint endPoint)
{
    m_beginPoint = beginPoint;
    m_endPoint = endPoint;
}
