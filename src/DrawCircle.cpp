#include "DrawCircle.h"

#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include <math.h>

CDrawCircle::CDrawCircle() : IDrawObject(TYPE_DRAW_CIRCLE)
{
}

CDrawCircle::~CDrawCircle()
{
}

void CDrawCircle::Draw()
{
    CPoint p1 = m_beginPoint;
    CPoint p2((m_beginPoint.x + m_endPoint.x) /2, (m_beginPoint.y + m_endPoint.y) /2 , 0 );
    CPoint p3 = m_endPoint;

    CalculateCircle(m_beginPoint, p2, m_endPoint, &m_Xs, &m_Ys, &m_r);

    glColor3f(0.f, 0.0f, 1.0f);
    glBegin(GL_LINE_LOOP);
    float x,y;
    for (float i = 0  ; i <= 2 * PI ; i+= 0.1f)
    {
        x= m_Xs + m_r * cos(i);
        y = m_Ys + m_r * sin(i);
        glVertex2f(x,y);
    }
    glEnd();
}

void CDrawCircle::CalculateCircle(const CPoint& p1, const CPoint& p2, const CPoint& p3, float* Xs, float* Ys, float* r)
{
    *Xs = 0.5 * ( (pow(p2.x, 2) * p3.y + pow(p2.y,2) * p3.y - pow(p1.x,2) *p3.y - pow(p1.y, 2) * p3.y) / (p1.y*p3.x - p1.y* p2.x+ p2.y*p1.x- p2.y*p3.x+ p3.y*p2.x- p3.y*p1.x) +
    (pow(p1.x, 2) * p2.y + pow(p1.y,2) * p2.y - pow(p3.x,2) *p2.y - pow(p3.y, 2) * p2.y) / (p1.y*p3.x - p1.y* p2.x+ p2.y*p1.x- p2.y*p3.x+ p3.y*p2.x- p3.y*p1.x) +
    (pow(p3.x, 2) * p1.y + pow(p3.y,2) * p1.y - pow(p2.x,2) *p1.y - pow(p2.y, 2) * p1.y) / (p1.y*p3.x - p1.y* p2.x+ p2.y*p1.x- p2.y*p3.x+ p3.y*p2.x- p3.y*p1.x) );

     *Ys = 0.5 * (
                     (pow(p1.x, 2) * p3.x + pow(p1.y,2) * p3.x - pow(p2.x,2) *p3.x - pow(p2.y, 2) * p3.x) / (p1.y*p3.x - p1.y* p2.x+ p2.y*p1.x- p2.y*p3.x+ p3.y*p2.x- p3.y*p1.x) +
                     (pow(p3.x, 2) * p2.x + pow(p3.y,2) * p2.x - pow(p1.x,2) *p2.x - pow(p1.y, 2) * p2.x) / (p1.y*p3.x - p1.y* p2.x+ p2.y*p1.x- p2.y*p3.x+ p3.y*p2.x- p3.y*p1.x) +
                     (pow(p2.x, 2) * p1.x + pow(p2.y,2) * p1.x - pow(p3.x,2) *p1.x - pow(p3.y, 2) * p1.x) / (p1.y*p3.x - p1.y* p2.x+ p2.y*p1.x- p2.y*p3.x+ p3.y*p2.x- p3.y*p1.x)
                     );

    *r = sqrt(  pow(p1.x - *Xs,2) + pow(p1.y - *Ys,2));
}


void CDrawCircle::CurrentDraw()
{
    glColor3f(1.0f, 0.0f, 1.0f);
    glBegin(GL_LINES);
    glVertex2f(m_beginPoint.x,   m_beginPoint.y);
    glVertex2f(m_endPoint.x,   m_endPoint.y);
    glEnd();

    CPoint p1 = m_beginPoint;
    CPoint p2((m_beginPoint.x + m_endPoint.x) /2, (m_beginPoint.y + m_endPoint.y) /2 , 0 );
    CPoint p3 = m_endPoint;

    CalculateCircle(m_beginPoint, p2, m_endPoint, &m_Xs, &m_Ys, &m_r);

    glColor3f(0.f, 0.0f, 1.0f);
    glBegin(GL_LINE_LOOP);
    float x,y;
    for (float i = 0  ; i <= 2 * PI ; i+= 0.10f)
    {
        x = m_Xs + m_r * cos(i);
        y = m_Ys + m_r * sin(i);
        glVertex2f(x,y);
    }
    glEnd();
}



