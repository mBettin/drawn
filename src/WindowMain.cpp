#include "App.h"
#include "resource.h"
#include "WindowMain.h"


#include <iostream>
#include <Windowsx.h>

CWindowMain::CWindowMain() : m_engineDraw(m_width, m_height)
{
    m_width = 800;
    m_height = 600;
    m_name = "MainWindow";
    points = new std::vector<POINT>();
}

CWindowMain::~CWindowMain()
{
    //dtor
}

bool CWindowMain::Create(const HINSTANCE& hInstance)
{
    WNDCLASSEX wcex;
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_OWNDC;
    wcex.lpfnWndProc = WindowProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = "ClassDraw";
    wcex.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

    if (!RegisterClassEx(&wcex))
        return false;

    /* create main window */
    m_hwnd = CreateWindowEx(0,
                            "ClassDraw",
                            m_name.c_str(),
                            WS_OVERLAPPEDWINDOW,
                            CW_USEDEFAULT,
                            CW_USEDEFAULT,
                            m_width,
                            m_height,
                            NULL,
                            NULL,
                            hInstance,
                            NULL);
    return true;
}

void CWindowMain::EnableOpenGL(HDC* hDC, HGLRC* hRC)
{
    PIXELFORMATDESCRIPTOR pfd;

    int iFormat;

    /* get the device context (DC) */
    *hDC = GetDC(m_hwnd);

    /* set the pixel format for the DC */
    ZeroMemory(&pfd, sizeof(pfd));

    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW |
                  PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 24;
    pfd.cDepthBits = 16;
    pfd.iLayerType = PFD_MAIN_PLANE;

    iFormat = ChoosePixelFormat(*hDC, &pfd);

    SetPixelFormat(*hDC, iFormat, &pfd);

    *hRC = wglCreateContext(*hDC);

    wglMakeCurrent(*hDC, *hRC);

    m_engineDraw.InitScene();

    OnResize(m_width, m_height);
}

void CWindowMain::DisableOpenGL(HDC hDC, HGLRC hRC)
{
    wglMakeCurrent(NULL, NULL);
    wglDeleteContext(hRC);
    ReleaseDC(m_hwnd, hDC);
}

void CWindowMain::Show(const int& nCmdShows)
{
    ShowWindow(m_hwnd, nCmdShows);
}

void CWindowMain::Destroy()
{
    DestroyWindow(m_hwnd);
}

void CWindowMain::Display()
{
    m_engineDraw.Display();
}

void CWindowMain::OnUndo()
{
    m_engineDraw.Undo();
}

void CWindowMain::OnMouseMove(const WPARAM& wParam, const LPARAM& lParam)
{
    int xPos = GET_X_LPARAM(lParam);
    int yPos = GET_Y_LPARAM(lParam);

    CPoint p = m_engineDraw.ConvertPosition(xPos, yPos);
    m_engineDraw.SetCurrentPoint(p);
}

void CWindowMain::OnMouseClick(const WPARAM& wParam, const LPARAM& lParam)
{
    int xPos = GET_X_LPARAM(lParam);
    int yPos = GET_Y_LPARAM(lParam);

    CPoint p = m_engineDraw.ConvertPosition(xPos, yPos);

    std::cout << "xPos:" << p.x << ", yPos:" << p.y << "\n";
    m_engineDraw.AddDrawObject();
    m_engineDraw.SetLastPoint();

    //m_engineDraw.SetCurrentPoint(p);
}

void CWindowMain::OnResize(const int& width, const int& height)
{
    m_width = width;
    m_height = height;
    m_engineDraw.Reshape(m_width, m_height);
}

void CWindowMain::OnCreate(const HWND& hwnd)
{

}

void CWindowMain::OnContinue()
{
    m_engineDraw.Continue();
}

void CWindowMain::ChangeDrawingObject(const TYPE_DRAW& typeDraw)
{
    m_engineDraw.SetCurrentTypeDraw(typeDraw);
    //m_engineDraw.ClearDraw();
}

bool CWindowMain::GetStatusUndo()
{
    return m_engineDraw.IsDraw();
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
    case WM_CLOSE:
        PostQuitMessage(0);
        break;

    case WM_DESTROY:
        return 0;

    case WM_KEYDOWN:
    {
        switch (wParam)
        {
        case VK_ESCAPE:
            PostQuitMessage(0);
            break;
        }
    }
    break;

    case WM_MOUSEMOVE:
    {
        CApp::GetInstance().GetWindowMain()->OnMouseMove(wParam, lParam);
        break;
    }

    case WM_LBUTTONUP:
    {
        CApp::GetInstance().GetWindowMain()->OnMouseClick(wParam, lParam);
        break;
    }
    case WM_RBUTTONUP:
    {
        POINT point;
        point.x = LOWORD(lParam);
        point.y = HIWORD(lParam);

        HMENU m_popupMenu;
        m_popupMenu = CreatePopupMenu();
        ClientToScreen(hwnd, &point);


        AppendMenuW(m_popupMenu, MF_STRING, ID_POP_MENU_LINE, L"&Line");
        AppendMenuW(m_popupMenu, MF_STRING, ID_POP_MENU_ARC, L"&Arc - NURSE");
        AppendMenuW(m_popupMenu, MF_STRING, ID_POP_MENU_CIRCE, L"&Circle");
        AppendMenuW(m_popupMenu, MF_SEPARATOR, 0, NULL);

        if (CApp::GetInstance().GetWindowMain()->GetStatusUndo())
        {
            AppendMenuW(m_popupMenu, MF_STRING, ID_POP_MENU_FINISH, L"&Finish");
        }
        else
        {
            AppendMenuW(m_popupMenu, MF_STRING | MF_DISABLED, ID_POP_MENU_FINISH, L"&Finish");
        }
        if(CApp::GetInstance().GetWindowMain()->GetStatusUndo())
        {
            AppendMenuW(m_popupMenu, MF_STRING, ID_POP_MENU_CONTINUE, L"&Continue");
        }
        else
        {
            AppendMenuW(m_popupMenu, MF_STRING | MF_DISABLED   , ID_POP_MENU_CONTINUE, L"&Continue");
        }
        AppendMenuW(m_popupMenu, MF_SEPARATOR, 0, NULL);
        if (CApp::GetInstance().GetWindowMain()->GetStatusUndo())
        {
             AppendMenuW(m_popupMenu, MF_STRING, ID_POP_MENU_UNDO, L"&Undo");
        }
        else
        {
             AppendMenuW(m_popupMenu, MF_STRING | MF_DISABLED , ID_POP_MENU_UNDO, L"&Undo");
        }
        TrackPopupMenu(m_popupMenu, TPM_RIGHTBUTTON, point.x, point.y, 0, hwnd, NULL);
        DestroyMenu(m_popupMenu);
        break;
    }
    case WM_SIZE:
    {
        CApp::GetInstance().GetWindowMain()->OnResize(LOWORD(lParam), HIWORD(lParam));
        break;
    }
    case WM_CREATE:
    {
        CApp::GetInstance().GetWindowMain()->OnCreate(hwnd);
    }
      case WM_COMMAND:
        switch(LOWORD(wParam)) {
            case ID_POP_MENU_UNDO:
                {
                    CApp::GetInstance().GetWindowMain()->OnUndo();
                    break;
                }

            case ID_POP_MENU_CONTINUE:
                {
                    CApp::GetInstance().GetWindowMain()->OnContinue();
                    break;
                }
            case ID_POP_MENU_FINISH:
                {
                    CApp::GetInstance().GetWindowMain()->ChangeDrawingObject(TYPE_DRAW_NONE);
                    break;
                }
            case ID_POP_MENU_ARC:
                {
                    CApp::GetInstance().GetWindowMain()->ChangeDrawingObject(TYPE_DRAW_ARC);
                    break;
                }
            case ID_POP_MENU_CIRCE:
                {
                    CApp::GetInstance().GetWindowMain()->ChangeDrawingObject(TYPE_DRAW_CIRCLE);
                    break;
                }
            case ID_POP_MENU_LINE:
                {
                    CApp::GetInstance().GetWindowMain()->ChangeDrawingObject(TYPE_DRAW_LINE);
                    break;
                }
        }
        break;
    default:
        return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }

    return 0;
}

