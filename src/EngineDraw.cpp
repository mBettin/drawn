#include "EngineDraw.h"
#include <math.h>

CEngineDraw::CEngineDraw(const int& width, const int& height)
{
    m_width = &width;
    m_height = &height;
    m_drawObjects = new std::vector<IDrawObject*>();
    m_currentTypeDraw = TYPE_DRAW_LINE;
    DisabledDraw();
    m_aspect = 1;

    line = new CDrawLine();
    arc = new CDrawArc();
    circle = new CDrawCircle();
}

CEngineDraw::~CEngineDraw()
{
    delete line;
    delete arc;
    delete circle;
    for (std::vector<IDrawObject*>::iterator it= m_drawObjects->begin(); it != m_drawObjects->end(); ++it)
    {
        delete *it;
    }
    delete m_drawObjects;
}

void CEngineDraw::InitScene()
{

}

void CEngineDraw::Continue()
{
    EnableDraw();
}

void CEngineDraw::Reshape(const int& width, const int& height)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glViewport(0, 0, width, height);

    if (height > 0)
    {
        m_aspect = (GLfloat)width / (GLfloat)height;
    }

    if (width >= height)
    {
        gluOrtho2D(-1.0 * m_aspect, 1.0 * m_aspect, -1.0, 1.0);
    }
    else
    {
        gluOrtho2D(-1.0, 1.0, -1.0 / m_aspect, 1.0 / m_aspect);
    }

    gluLookAt( 1.0f * m_aspect, - 1.0f, 0.0f,
               1.0f * m_aspect,  - 1.0f, - 100.0f,
               0.0f, 1.0f, 0.0f );

    m_width = &width;
    m_height = &height;
}

void CEngineDraw::AddDrawObject()
{
    if (m_isDraw)
    {
        if (TYPE_DRAW_LINE == m_currentTypeDraw)
        {
            CDrawLine* drawObject = new CDrawLine();
            drawObject->SetPoints(m_lastPoint, m_currentPoint);
            drawObject->SetTypeDraw(TYPE_DRAW_LINE);
            m_drawObjects->push_back(drawObject);
        }
        else if ( TYPE_DRAW_ARC == m_currentTypeDraw)
        {
            CDrawArc* drawArc = new CDrawArc();
            drawArc->SetPoints(m_lastPoint, m_currentPoint);
            drawArc->SetTypeDraw(TYPE_DRAW_ARC);
            m_drawObjects->push_back(drawArc);
        }
        else if (TYPE_DRAW_CIRCLE == m_currentTypeDraw)
        {
            CDrawCircle* drawCircle = new CDrawCircle();
            drawCircle->SetPoints(m_lastPoint, m_currentPoint);
            drawCircle->SetTypeDraw(TYPE_DRAW_CIRCLE);
            m_drawObjects->push_back(drawCircle);
        }
    }
    else
    {
        ClearDraw();
        if (TYPE_DRAW_NONE == m_currentTypeDraw)
        {
                SetCurrentTypeDraw(TYPE_DRAW_LINE);
        }
        EnableDraw();
    }
}

void CEngineDraw::Undo()
{
    IDrawObject* undoDrawObject =  m_drawObjects->back();
    m_currentTypeDraw = undoDrawObject->GetTypeDraw();
    m_lastPoint = undoDrawObject->GetBeginPoint();
    m_drawObjects->pop_back();
}

void CEngineDraw::ClearDraw()
{
        m_isDraw = false;
        m_drawObjects->clear();
}

void CEngineDraw::EnableDraw()
{
    m_isDraw = true;
}

void CEngineDraw::DisabledDraw()
{
    m_isDraw = false;
}


void CEngineDraw::Display()
{
    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    for (std::vector<IDrawObject*>::iterator it= m_drawObjects->begin(); it != m_drawObjects->end(); ++it)
    {
        (*it)->Draw();
    }

    if(m_isDraw)
    {
        if (TYPE_DRAW_LINE == m_currentTypeDraw)
        {
            line->SetPoints(m_lastPoint, m_currentPoint);
            line->CurrentDraw();
        }
        else if (TYPE_DRAW_ARC == m_currentTypeDraw)
        {
            arc->SetPoints(m_lastPoint, m_currentPoint);
            arc->CurrentDraw();
        }
        else if (TYPE_DRAW_CIRCLE == m_currentTypeDraw)
        {
            circle->SetPoints(m_lastPoint, m_currentPoint);
            circle->CurrentDraw();
        }
    }

}

bool CEngineDraw::IsDraw()
{
    return m_isDraw;
}

CPoint CEngineDraw::ConvertPosition(const int& x, const int& y)
{
    double projection[16];
    double modelview[16];

    int viewport[4];
    float z = 1;

    glGetDoublev( GL_PROJECTION_MATRIX, projection );
    glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
    glGetIntegerv( GL_VIEWPORT, viewport );

    glReadPixels( x, viewport[3]-y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &z );

    CPoint p;
    gluUnProject( x, viewport[3]-y, z, modelview, projection, viewport, &p.x, &p.y, &p.z );


    return p;
}
