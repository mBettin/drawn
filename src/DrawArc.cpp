#include "DrawArc.h"

#include <math.h>

CDrawArc::CDrawArc() : IDrawObject(TYPE_DRAW_ARC)
{
    m_knots = NULL;
    m_controlPoints = NULL;
    Init(3);
    m_nurbs = gluNewNurbsRenderer();
}

CDrawArc::~CDrawArc()
{
    delete m_knots;
    delete m_controlPoints;
    gluDeleteNurbsRenderer(m_nurbs);
}

void CDrawArc::Draw()
{
    glColor3f(0.0f, 0.0f, 1.0f);
    gluBeginCurve( m_nurbs );
    gluNurbsProperty( m_nurbs, GLU_SAMPLING_TOLERANCE, 10.0 );
    gluNurbsCurve( m_nurbs, m_countControlPoints * 2, m_knots, VERTEX_3, m_controlPoints, m_countControlPoints, GL_MAP1_VERTEX_3 );
    gluEndCurve( m_nurbs );
}

#define w1 1.0
#define w2 0.707

GLfloat knots[12]={ 0.0,0.0,0.0, 0.25,0.25, 0.5,0.5, 0.75,0.75,
1.0,1.0,1.0 };
GLfloat ctlpoints[9][4] = {
 w1*1.0, w1*-2.0, w1*0.0, w1,
 w2*2.0, w2*-2.0, w2*0.0, w2,
 w1*2.0,  w1*-1.0, w1*0.0, w1,
 w2*2.0,  w2*0.0, w2*0.0, w2,
 w1*1.0,  w1*0.0, w1*0.0, w1,
 w2*0.0,  w2*0.0, w2*0.0, w2,
 w1*0.0,  w1*-1.0, w1*0.0, w1,
 w2*0.0, w2*-2.0, w2*0.0, w2,
 w1*1.0, w1*-2.0, w1*0.0, w1
};

void CDrawArc::CurrentDraw()
{
    glColor3f(0.0f, 0.0f, 1.0f);
    gluBeginCurve( m_nurbs );
    gluNurbsProperty( m_nurbs, GLU_SAMPLING_TOLERANCE, 10.0 );
    //gluNurbsCurve( m_nurbs, m_countControlPoints * 2, m_knots, VERTEX_3, m_controlPoints, m_countControlPoints, GL_MAP1_VERTEX_3 );


       gluNurbsCurve(m_nurbs,
                 12, knots,
                 4,
                 &ctlpoints[0][0],
                 3,
                 GL_MAP1_VERTEX_4);
gluEndCurve( m_nurbs );
    glColor3f(1.0f, 0.0f, 0.0f);
    glBegin(GL_LINE_LOOP);
    for (float i = 0 ; i < TPI ; i+=0.1f)
    {
        float x = m_controlPoints[3] + 0.005 * cos(i);
        float y = m_controlPoints[4] + 0.005 * sin(i);
        glVertex2f(x,y);
    }
    glEnd();
}

void CDrawArc::SetPoints(CPoint beginPoint, CPoint endPoint)
{
    m_beginPoint = beginPoint;
    m_endPoint = endPoint;

    m_controlPoints[0] = m_beginPoint.x;
    m_controlPoints[1] = m_beginPoint.y;
    m_controlPoints[2] = m_beginPoint.z;

    ctlpoints[0][0] = m_beginPoint.x;
    ctlpoints[0][1] = m_beginPoint.y;

    ctlpoints[8][0] = m_beginPoint.x;
    ctlpoints[8][1] = m_beginPoint.y;

    int i = 1;
    for ( ; i < m_countControlPoints - 1 ; i+=VERTEX_3)
    {
        m_controlPoints[i*VERTEX_3] = m_beginPoint.x + i * ((m_endPoint.x - m_beginPoint.x) / (float) (m_countControlPoints - 1));
        m_controlPoints[i*VERTEX_3 + 1] = m_beginPoint.y + (i * ((m_endPoint.y - m_beginPoint.y) / (float) (m_countControlPoints - 1))) + (0.3);
        m_controlPoints[i*VERTEX_3 + 2] = 0;
    }

    m_controlPoints[ m_countControlPoints * VERTEX_3 - 3] = m_endPoint.x;
    m_controlPoints[ m_countControlPoints * VERTEX_3 - 2] = m_endPoint.y;
    m_controlPoints[ m_countControlPoints * VERTEX_3 - 1] = m_endPoint.z;
}

void CDrawArc::Init(int nCountControlPoints)
{
    m_controlPoints = new GLfloat[nCountControlPoints * VERTEX_3];
    m_knots = new GLfloat[nCountControlPoints * 2];

    int i = 0;
    for ( ; i < nCountControlPoints  ; i++)
    {
            m_knots[i] = 0.0f;
    }
    for ( i = nCountControlPoints  ; i < nCountControlPoints*2; i++)
    {
        m_knots[i] = 1.0f;
    }

    m_countControlPoints = nCountControlPoints;
}
