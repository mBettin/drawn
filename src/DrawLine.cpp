#include "DrawLine.h"

#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>

CDrawLine::CDrawLine() : IDrawObject(TYPE_DRAW_LINE)
{
}

CDrawLine::~CDrawLine()
{
}

void CDrawLine::Draw()
{
    glColor3f(0.0f, 0.0f, 1.0f);
    glBegin(GL_LINES);
    glVertex2f(m_beginPoint.x,   m_beginPoint.y);
    glVertex2f(m_endPoint.x,   m_endPoint.y);
    glEnd();
}

void CDrawLine::CurrentDraw()
{
    Draw();
}
