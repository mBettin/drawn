#include "App.h"

CApp::CApp()
{
    m_windowMain = new CWindowMain;
}

CApp::~CApp()
{
    delete m_windowMain;
}

CApp& CApp::GetInstance()
{
    static CApp instance;
    return instance;
}

CWindowMain* CApp::GetWindowMain()
{
    return m_windowMain;
}

