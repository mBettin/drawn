//---------------------------------------------------------------------------
#ifndef VS
#include <vcl.h>
#endif // VS

#include <windows.h>

#include "App.h"

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine,
                   int nCmdShow)
{
    CApp::GetInstance().GetWindowMain()->Create(hInstance);

    HDC hDC;
    HGLRC hRC;
    MSG msg;
    BOOL bQuit = false;


    CApp::GetInstance().GetWindowMain()->Show(nCmdShow);
    CApp::GetInstance().GetWindowMain()->EnableOpenGL(&hDC, &hRC);

    while (!bQuit)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
            {
                bQuit = TRUE;
            }
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
        else
        {
            CApp::GetInstance().GetWindowMain()->Display();
            SwapBuffers(hDC);
            Sleep (1);
        }
    }

    CApp::GetInstance().GetWindowMain()->DisableOpenGL(hDC, hRC);
    CApp::GetInstance().GetWindowMain()->Destroy();

    return msg.wParam;
}






